provider "aws" {
  region = "ap-southeast-1"
}

# Create VPC for the EKS cluster
module "vpc_eks" {
  source                     = "../../modules/vpc"
  project                    = var.project
  environment                = var.environment
  default_tags               = var.default_tags
  availability_zones_public  = ["ap-southeast-1a", "ap-southeast-1b", "ap-southeast-1c"]
  availability_zones_private = ["ap-southeast-1a", "ap-southeast-1b", "ap-southeast-1c"]
  cidr_block_internet_gw     = "0.0.0.0/0"
  cidr_block_nat_gw          = "0.0.0.0/0"
  vpc_name                   = "eks"
  vpc_cidr                   = "172.16.0.0/16"
  public_subnets_cidr        = ["172.16.1.0/24", "172.16.2.0/24", "172.16.3.0/24"]
  private_subnets_cidr       = ["172.16.10.0/24", "172.16.20.0/24", "172.16.30.0/24"]
}

# Create VPC for the RDS instance
module "vpc_rds" {
  source                     = "../../modules/vpc"
  project                    = var.project
  environment                = var.environment
  default_tags               = var.default_tags
  availability_zones_public  = ["ap-southeast-1a", "ap-southeast-1b", "ap-southeast-1c"]
  availability_zones_private = ["ap-southeast-1a", "ap-southeast-1b", "ap-southeast-1c"]
  cidr_block_internet_gw     = "0.0.0.0/0"
  cidr_block_nat_gw          = "0.0.0.0/0"
  vpc_name                   = "rds"
  vpc_cidr                   = "172.32.0.0/16"
  public_subnets_cidr        = ["172.32.1.0/24", "172.32.2.0/24", "172.32.3.0/24"]
  private_subnets_cidr       = ["172.32.10.0/24", "172.32.20.0/24", "172.32.30.0/24"]
}

# Peering 2 VPC for EKS clusters and RDS instances
module "vpc_peering" {
  source                      = "../../modules/vpc-peering"
  project                     = var.project
  environment                 = var.environment
  default_tags                = var.default_tags
  vpc_id                      = module.vpc_eks.vpc_id
  peer_vpc_id                 = module.vpc_rds.vpc_id
  route_table_id_vpc1         = module.vpc_eks.nat_route_table
  route_table_id_vpc2         = module.vpc_rds.nat_route_table
  destination_cidr_block_vpc1 = module.vpc_eks.cidr_block
  destination_cidr_block_vpc2 = module.vpc_rds.cidr_block
}

# Create a private ECR repository to store images of Laravel applications
module "ecr" {
  source          = "../../modules/ecr"
  project         = var.project
  environment     = var.environment
  default_tags    = var.default_tags
  repository_name = "laravel"
}

# Create an EKS cluster in the private subnet of the VPC for EKS
module "eks" {
  source                      = "../../modules/eks"
  project                     = var.project
  environment                 = var.environment
  default_tags                = var.default_tags
  cluster_name                = "laravel"
  private_subnets             = module.vpc_eks.aws_subnets_private
  node_group_name             = "spot-node-group"
  node_group_desired_capacity = 1
  node_group_min_size         = 1
  node_group_max_size         = 3
  node_instance_type          = "t2.micro"
}

# Create an RDS instance in the private subnet of the VPC for RDS
module "rds" {
  source                = "../../modules/rds"
  project               = var.project
  environment           = var.environment
  default_tags          = var.default_tags
  vpc_id                = module.vpc_rds.vpc_id
  private_subnets       = module.vpc_rds.aws_subnets_private
  allocated_storage     = 20
  max_allocated_storage = 100
  engine                = "mysql"
  engine_version        = "8.0.35"
  instance_name         = "rds-lavarel"
  instance_class        = "db.t2.micro"
  db_name               = "laravel"
  username              = "laravelmaster"
}
