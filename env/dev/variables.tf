variable "region" {
  type    = string
  default = "ap-southeast-1"
}

variable "project" {
  type    = string
  default = "devopslite"
}

variable "environment" {
  type    = string
  default = "dev"
}

variable "default_tags" {
  type = map(string)
  default = {
    Environment = "Development"
    Provisioner = "Terraform"
    Project     = "DevOpsLite"
  }
}
