output "eks_cluster_id" {
  description = "Name of the cluster"
  value       = module.eks.eks_cluster_id
}

output "eks_cluster_endpoint" {
  description = "Endpoint for your Kubernetes API server"
  value       = module.eks.eks_cluster_endpoint
}

output "kubeconfig_certificate_authority_data" {
  description = "Base64 encoded certificate data required to communicate with your cluster. Add this to the 'certificate-authority-data' section of the 'kubeconfig' file for your cluster"
  value       = module.eks.kubeconfig_certificate_authority_data
}

output "rds_endpoint" {
  description = "The connection endpoint in address:port format"
  value       = module.rds.rds_endpoint
}

output "rds_address" {
  description = "The hostname of the RDS instance"
  value       = module.rds.rds_address
}

output "rds_db_name" {
  description = "The database name"
  value       = module.rds.rds_db_name
}

output "rds_username" {
  description = "The master username for the database"
  value       = module.rds.rds_username
}

output "rds_master_user_secret" {
  description = "A block that specifies the master user secret"
  value       = module.rds.rds_master_user_secret
}

output "repository_url" {
  description = "The URL of the repository (in the form aws_account_id.dkr.ecr.region.amazonaws.com/repositoryName)"
  value       = module.ecr.repository_url
}
