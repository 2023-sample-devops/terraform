# How to use this repository?

This repository is used to:
- Create 2 VPCs in the Singapore region
- Peering 2 VPCs together
- Create an EKS cluster with spot instance and auto scaling in the first VPC, located in a private subnet
- Create an RDS instance with multi az in the second VPC, located in a private subnet and only allow RDS access from VPC EKS
- Create a private repository on ECR to store the application image

## Table of contents

- [1. Structure of the repository](#1-structure-of-the-repository)
- [2. Steps to set up this repository](#2-steps-to-set-up-this-repository)
    - [2.1 Create an S3 bucket to store Terraform state](#21-create-an-s3-bucket-to-store-terraform-state)
    - [2.2 Create AWS resources with Gitlab CI](#22-create-aws-resources-with-gitlab-ci)
- [3. Clean up](#3-clean-up)
    - [3.1 Delete the main resources](#31-delete-the-main-resources)
    - [3.2 Delete the S3 bucket used to store state](#32-delete-the-s3-bucket-used-to-store-state)

## 1. Structure of the repository

Below is an explanation of the directories and files contained in the repository.

![structure](./images/structure.jpg)

## 2. Steps to set up this repository

### 2.1 Create an S3 bucket to store Terraform state

On your computer, Terraform and AWS CLI need to be installed. You can refer to the information below.

- https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html
- https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html
- https://www.terraform.io/downloads.html

I assume that you have successfully set up Terraform and AWS CLI on your machine, next move to the [backend](./backend/) directory.

Open the [main.tf](./backend/main.tf) file and then comment lines `9-15`. Then execute the following 2 commands to create an S3 bucket.

```sh
terraform init
terraform apply
```

After creating the S3 bucket, uncomment lines 9-15 in the main.tf file that we just mentioned above.

Then, run the command below to move the state of the backend directory up to the bucket we just created.

```sh
terraform init -migrate-state
```

You can now delete the `terraform.tfstate` file and the `terraform.tfstate.backup` file in the `backend` folder, on your computer.

### 2.2 Create AWS resources with Gitlab CI

Once we have an S3 bucket to store state for Terrafom, we will use Gitlab CI to automate the creation/update/delete of AWS resources declared in the [env](./env/) directory.

You need to go to `Settings -> CI/CD -> Variables`. Then create 2 variables as the image below, which are the IAM key and IAM secret that you will use for Gitlab CI, so that it can be operated on AWS. This IAM account will need administrator rights and so you should keep it safe.

![gitlab_vairables](./images/gitlab_variables.jpg)

Now, every time we update Terraform code, CI will be automatically performed. It will do the following:
- Check the format of `.tf` files
- Validate Terraform code
- Run the plan and save the results to an artifact
- Manually perform the apply

![pipeline](./images/pipeline.jpg)
![pipeline_plan](./images/pipeline_plan.jpg)

## 3. Clean up

To clean up all the resources on AWS that you created with Terraform, we will have to do it on your computer.

For safety reasons, I do not set up the `destroy` step using CI. I want to do it manually when I need it.

### 3.1 Delete the main resources

Now move into the [env/dev](./env/dev/) directory and run the following command, it will remove all the resources created for the dev environment.

```sh
terraform init
terraform destroy
```

### 3.2 Delete the S3 bucket used to store state

You need to move into the [backend](./backend/) directory and run the following command to download the state from S3 to your computer.

```sh
terraform state pull > terraform.tfstate
```

Then open the [backend/main.tf](./backend/main.tf) file and comment lines `9-15`.

Delete the local terraform cache folder.

```sh
rm -rf .terraform
```

Then delete all resources related to the S3 bucket, including DynamoDB.

```sh
terraform init
terraform destroy
```