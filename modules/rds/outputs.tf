output "rds_endpoint" {
  description = "The connection endpoint in address:port format"
  value       = aws_db_instance.rds_instance.endpoint
}

output "rds_address" {
  description = "The hostname of the RDS instance"
  value       = aws_db_instance.rds_instance.address
}

output "rds_db_name" {
  description = "The database name"
  value       = aws_db_instance.rds_instance.db_name
}

output "rds_username" {
  description = "The master username for the database"
  value       = aws_db_instance.rds_instance.username
}

output "rds_master_user_secret" {
  description = "A block that specifies the master user secret"
  value       = aws_db_instance.rds_instance.master_user_secret
}
