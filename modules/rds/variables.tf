variable "vpc_id" {
  description = "ID of the VPC used for the RDS instance"
  type        = string
}

variable "private_subnets" {
  description = "List of private subnets used for EKS cluster"
  type        = list(string)
}

variable "allocated_storage" {
  description = "The allocated storage in gibibytes"
  type        = number
}

variable "max_allocated_storage" {
  description = "When configured, the upper limit to which Amazon RDS can automatically scale the storage of the DB instance"
  type        = number
}

variable "engine" {
  description = "The database engine to use"
  type        = string
}

variable "engine_version" {
  description = "The engine version to use"
  type        = string
}

variable "instance_name" {
  description = "The name of the RDS instance"
  type        = string
}

variable "instance_class" {
  description = "The instance type of the RDS instance"
  type        = string
}

variable "db_name" {
  description = "The database name"
  type        = string
}

variable "username" {
  description = "The master username for the database"
  type        = string
}

variable "environment" {
  description = "Environment name used, for example: dev, staging, sandbox, uat, production"
  type        = string
}

variable "project" {
  description = "Name of the project"
  type        = string
}

variable "default_tags" {
  description = "List of default tags to be used for each environment"
  type        = map(string)
}
