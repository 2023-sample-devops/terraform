# Create a Security Group for the RDS instance
resource "aws_security_group" "rds_sg" {
  name        = "${var.project}-${var.environment}-${var.instance_name}-rds-sg"
  description = "Security group for the RDS ${var.project}-${var.environment}-${var.instance_name}"
  vpc_id      = var.vpc_id

  ingress {
    description = "Enables RDS access from the VPC EKS cluster"
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["172.16.0.0/16"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = merge(
    var.default_tags,
    {
      Name = "${var.project}-${var.environment}-rds-sg"
    }
  )
}

# Create RDS subnet group
resource "aws_db_subnet_group" "rds_subnet_group" {
  name       = "${var.project}-${var.environment}-${var.instance_name}-rds-subnet-group"
  subnet_ids = var.private_subnets

  tags = merge(
    var.default_tags,
    {
      Name = "${var.project}-${var.environment}-${var.instance_name}-rds-subnet-group"
    }
  )
}

# Create RDS instance
resource "aws_db_instance" "rds_instance" {
  allocated_storage            = var.allocated_storage
  max_allocated_storage        = var.max_allocated_storage
  engine                       = var.engine
  engine_version               = var.engine_version
  identifier                   = var.instance_name
  instance_class               = var.instance_class
  db_name                      = var.db_name
  username                     = var.username
  allow_major_version_upgrade  = false
  apply_immediately            = true
  auto_minor_version_upgrade   = true
  deletion_protection          = true
  manage_master_user_password  = true
  multi_az                     = true
  performance_insights_enabled = false
  storage_encrypted            = true
  publicly_accessible          = false
  backup_retention_period      = 7
  db_subnet_group_name         = aws_db_subnet_group.rds_subnet_group.name
  vpc_security_group_ids       = [aws_security_group.rds_sg.id]

  depends_on = [
    aws_db_subnet_group.rds_subnet_group,
    aws_security_group.rds_sg
  ]

  tags = merge(
    var.default_tags,
    {
      Name = "${var.project}-${var.environment}-${var.instance_name}-rds-subnet-group"
    }
  )
}
