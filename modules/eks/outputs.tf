output "eks_cluster_id" {
  description = "Name of the cluster"
  value       = aws_eks_cluster.eks_cluster.id
}

output "eks_cluster_endpoint" {
  description = "Endpoint for your Kubernetes API server"
  value       = aws_eks_cluster.eks_cluster.endpoint
}

output "kubeconfig_certificate_authority_data" {
  description = "Base64 encoded certificate data required to communicate with your cluster. Add this to the 'certificate-authority-data' section of the 'kubeconfig' file for your cluster"
  value       = aws_eks_cluster.eks_cluster.certificate_authority[0].data
}
