variable "cluster_name" {
  description = "Name of the EKS cluster"
  type        = string
}

variable "private_subnets" {
  description = "List of private subnets used for EKS cluster"
  type        = list(string)
}

variable "node_group_name" {
  description = "Name of the node group"
  type        = string
}

variable "node_group_desired_capacity" {
  description = "Number of desired spot instances"
  type        = number
}

variable "node_group_min_size" {
  description = "Minimum number of spot instances"
  type        = number
}

variable "node_group_max_size" {
  description = "Maximum number of spot instances"
  type        = number
}

variable "node_instance_type" {
  description = "Instance type of node group"
}

variable "default_tags" {
  description = "List of default tags to be used for each environment"
  type        = map(string)
}

variable "project" {
  description = "Name of the project"
  type        = string
}

variable "environment" {
  description = "Environment name used, for example: dev, staging, sandbox, uat, production"
  type        = string
}
