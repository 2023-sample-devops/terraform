data "aws_caller_identity" "peer" {
  provider = aws
}

# Create VPC Peering
resource "aws_vpc_peering_connection" "peering_vpc" {
  peer_owner_id = data.aws_caller_identity.peer.account_id
  vpc_id        = var.vpc_id
  peer_vpc_id   = var.peer_vpc_id
  auto_accept   = true

  tags = merge(
    var.default_tags,
    {
      Name = "${var.vpc_id}-peering-to-${var.peer_vpc_id}"
    }
  )
}

# Setup allows DNS resolution between 2 VPCs
resource "aws_vpc_peering_connection_options" "peering_options" {
  vpc_peering_connection_id = aws_vpc_peering_connection.peering_vpc.id

  requester {
    allow_remote_vpc_dns_resolution = true
  }

  accepter {
    allow_remote_vpc_dns_resolution = true
  }
}

# Add route from VPC 1 to CIDR of VPC 2
resource "aws_route" "route_vpc1_to_vpc2" {
  route_table_id            = var.route_table_id_vpc1
  destination_cidr_block    = var.destination_cidr_block_vpc2
  vpc_peering_connection_id = aws_vpc_peering_connection.peering_vpc.id
}

# Add route from VPC 2 to CIDR of VPC 1
resource "aws_route" "route_vpc2_to_vpc1" {
  route_table_id            = var.route_table_id_vpc2
  destination_cidr_block    = var.destination_cidr_block_vpc1
  vpc_peering_connection_id = aws_vpc_peering_connection.peering_vpc.id
}
