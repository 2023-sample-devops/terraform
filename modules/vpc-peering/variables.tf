variable "vpc_id" {
  description = "The id of the VPC requesting peering"
  type        = string
}

variable "peer_vpc_id" {
  description = "The id of the VPC accepting peering"
  type        = string
}

variable "default_tags" {
  description = "List of default tags to be used for each environment"
  type        = map(string)
}

variable "project" {
  description = "Name of the project"
  type        = string
}

variable "environment" {
  description = "Environment name used, for example: dev, staging, sandbox, uat, production"
  type        = string
}

variable "route_table_id_vpc1" {
  description = "The id of the route table of the VPC requesting peering"
  type        = string
}

variable "route_table_id_vpc2" {
  description = "The id of the route table of the VPC accepting peering"
  type        = string
}

variable "destination_cidr_block_vpc1" {
  description = "The CIDR of the VPC requesting peering"
  type        = string
}

variable "destination_cidr_block_vpc2" {
  description = "The CIDR of the VPC accepting peering"
  type        = string
}
