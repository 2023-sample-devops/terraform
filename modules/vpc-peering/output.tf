output "vpc_peering_id" {
  description = "The id of VPC peering"
  value       = aws_vpc_peering_connection.peering_vpc.id
}
