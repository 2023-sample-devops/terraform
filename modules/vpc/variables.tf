variable "vpc_name" {
  description = "Name of the VPC"
  type        = string
}

variable "vpc_cidr" {
  description = "The CDIR block used for the VPC"
  type        = string
}

variable "public_subnets_cidr" {
  description = "List of CIDRs used for public subnets"
  type        = list(string)
}

variable "private_subnets_cidr" {
  description = "List of CIDRs used for private subnets"
  type        = list(string)
}

variable "availability_zones_public" {
  description = "List of availability zones of public subnets"
  type        = list(string)
}

variable "availability_zones_private" {
  description = "List of availability zones of private subnets"
  type        = list(string)
}

variable "cidr_block_nat_gw" {
  description = "Destination CIRD of Nat Gateway"
  type        = string
}

variable "cidr_block_internet_gw" {
  description = "Destination CIDR of Internet Gateway"
  type        = string
}

variable "environment" {
  description = "Environment name used, for example: dev, staging, sandbox, uat, production"
  type        = string
}

variable "project" {
  description = "Name of the project"
  type        = string
}

variable "default_tags" {
  description = "List of default tags to be used for each environment"
  type        = map(string)
}
