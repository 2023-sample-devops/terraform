# Create the VPC
resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = merge(
    var.default_tags,
    {
      Name = "${var.project}-${var.environment}-${var.vpc_name}"
    }
  )
}

# Create the Public Subnets
resource "aws_subnet" "public_subnet" {
  count                   = length(var.public_subnets_cidr)
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = element(var.public_subnets_cidr, count.index)
  availability_zone       = element(var.availability_zones_public, count.index)
  map_public_ip_on_launch = true

  depends_on = [
    aws_vpc.vpc
  ]

  tags = merge(
    var.default_tags,
    {
      VPC   = "${var.project}-${var.environment}-${var.vpc_name}"
      State = "Public Subnet"
      Name  = "${var.project}-${var.environment}-${var.vpc_name}-public-${count.index}"
    }
  )
}

# Create the Private Subnets
resource "aws_subnet" "private_subnet" {
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = element(var.private_subnets_cidr, count.index)
  availability_zone = element(var.availability_zones_private, count.index)
  count             = length(var.private_subnets_cidr)

  depends_on = [
    aws_vpc.vpc
  ]

  tags = merge(
    var.default_tags,
    {
      VPC   = "${var.project}-${var.environment}-${var.vpc_name}"
      State = "Private Subnet"
      Name  = "${var.project}-${var.environment}-${var.vpc_name}-private-${count.index}"
    }
  )
}

# Create the Internet Gateway for the VPC
resource "aws_internet_gateway" "internet_gw" {
  vpc_id = aws_vpc.vpc.id

  depends_on = [
    aws_vpc.vpc
  ]

  tags = merge(
    var.default_tags,
    {
      VPC  = "${var.project}-${var.environment}-${var.vpc_name}"
      Name = "${var.project}-${var.environment}-${var.vpc_name}"
    }
  )
}

# Create an EIP for the NAT Gateway
resource "aws_eip" "eip_nat_gw" {
  domain           = "vpc"
  public_ipv4_pool = "amazon"

  tags = merge(
    var.default_tags,
    {
      Name = "${var.project}-${var.environment}-${var.vpc_name}-NAT-GW"
    }
  )
}

# Create the NAT Gateway with the first private subnet
resource "aws_nat_gateway" "nat_gw" {
  allocation_id = aws_eip.eip_nat_gw.id
  subnet_id     = element(aws_subnet.public_subnet.*.id, 0)

  depends_on = [
    aws_internet_gateway.internet_gw
  ]

  tags = merge(
    var.default_tags,
    {
      Name = "${var.project}-${var.environment}-${var.vpc_name}-NAT-GW"
    }
  )
}

# Create the Route Table for the public subnets
resource "aws_route_table" "internet_route_table" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = var.cidr_block_internet_gw
    gateway_id = aws_internet_gateway.internet_gw.id
  }

  depends_on = [
    aws_vpc.vpc
  ]

  tags = merge(
    var.default_tags,
    {
      Name  = "${var.project}-${var.environment}-${var.vpc_name}-public-route"
      State = "Public"
    }
  )
}

# Create the Route Table for the private subnets
resource "aws_route_table" "nat_route_table" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = var.cidr_block_nat_gw
    gateway_id = aws_nat_gateway.nat_gw.id
  }

  depends_on = [
    aws_vpc.vpc
  ]

  tags = merge(
    var.default_tags,
    {
      Name  = "${var.project}-${var.environment}-${var.vpc_name}-nat-route"
      State = "Private"
    }
  )
}

# Associate the public subnets with the route table "internet"
resource "aws_route_table_association" "route_association_public" {
  count          = length(var.public_subnets_cidr)
  subnet_id      = element(aws_subnet.public_subnet.*.id, count.index)
  route_table_id = aws_route_table.internet_route_table.id

  depends_on = [
    aws_route_table.internet_route_table,
    aws_subnet.public_subnet
  ]
}

# Associate the private subnets with the route table "nat"
resource "aws_route_table_association" "route_association_private" {
  count          = length(var.private_subnets_cidr)
  subnet_id      = element(aws_subnet.private_subnet.*.id, count.index)
  route_table_id = aws_route_table.nat_route_table.id

  depends_on = [
    aws_route_table.nat_route_table,
    aws_subnet.private_subnet
  ]
}
