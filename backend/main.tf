terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.24.0"
    }
  }

  backend "s3" {
    bucket         = "devopslite-terraform-state-bucket"
    key            = "s3-backend/terraform.tfstate"
    region         = "ap-southeast-1"
    encrypt        = true
    dynamodb_table = "devopslite-terraform-s3-backend"
  }
}

provider "aws" {
  region = "ap-southeast-1"
}

resource "aws_s3_bucket" "terraform_state_bucket" {
  bucket        = "${var.project}-terraform-state-bucket"
  force_destroy = true

  tags = merge(
    var.default_tags,
    {
      Name = "${var.project}-terraform-state-bucket"
    }
  )
}

resource "aws_s3_bucket_ownership_controls" "tf_bucket_ownership_controls" {
  depends_on = [
    aws_s3_bucket.terraform_state_bucket
  ]

  bucket = aws_s3_bucket.terraform_state_bucket.id

  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

resource "aws_s3_bucket_acl" "tf_bucket_acl" {
  depends_on = [
    aws_s3_bucket_ownership_controls.tf_bucket_ownership_controls
  ]

  bucket = aws_s3_bucket.terraform_state_bucket.id
  acl    = "private"
}

resource "aws_s3_bucket_versioning" "tf_bucket_versioning" {
  bucket = aws_s3_bucket.terraform_state_bucket.id

  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_kms_key" "tf_kms_key" {
  description             = "The KMS key is used to encrypt the S3 bucket as a backend for terraform"
  deletion_window_in_days = 7

  tags = merge(
    var.default_tags,
    {
      Name   = "${var.project}-terraform-s3-backend-key"
      Bucket = aws_s3_bucket.terraform_state_bucket.arn
    }
  )
}

resource "aws_s3_bucket_server_side_encryption_configuration" "tf_bucket_encryption" {
  bucket = aws_s3_bucket.terraform_state_bucket.id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm     = "aws:kms"
      kms_master_key_id = aws_kms_key.tf_kms_key.arn
    }
  }
}

resource "aws_dynamodb_table" "terraform_dynamodb_table" {
  name         = "${var.project}-terraform-s3-backend"
  hash_key     = "LockID"
  billing_mode = "PAY_PER_REQUEST"

  attribute {
    name = "LockID"
    type = "S"
  }

  tags = merge(
    var.default_tags,
    {
      Name   = "${var.project}-terraform-s3-backend"
      Bucket = aws_s3_bucket.terraform_state_bucket.arn
    }
  )
}
